> # **airwand.py**    
> **Requirements:**  
> Python 3.5 or newer  
> pip install bluepy  
> pip install pybluez\[ble\]  
> pip install mouse  
> pip install pynput  

> # **airwand.ino**  
> **Requirements**  
> Arduino IDE & Arduino Nano 33 BLE  
> Arduino.h library  
> ArduinoBLE.h library  
> Nano33BLEGyroscope.h library  
