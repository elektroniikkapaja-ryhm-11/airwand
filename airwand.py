from bluepy.btle import Peripheral, UUID, BTLEDisconnectError, DefaultDelegate

from bluetooth.ble import DiscoveryService

import mouse
from pynput.mouse import Button, Controller

import time

from _thread import *

class Wand():

    def __init__(self):
        self.data = []
        self.dts = []
        self.threshold = 1
        
        self.ratio_x = 16
        self.ratio_y = 9
        self.sensitivity = 1.5
        self.mouse = Controller()
        self.leftState = 0
        self.middleState = 0
        self.rightState = 0
        self.left = 0
        self.middle = 0
        self.right = 0

    def pushData(self, data, dt):
        self.data.append(data)
        self.dts.append(dt) 
    
    def handleMouseCoords(self, xdif, ydif, dt):
        dx = 0
        dy = 0
        #print("x: ", xdif, " y: ", ydif)

        if (abs(xdif) > self.threshold):
            dx = (xdif + 0.5)* dt * self.ratio_x * self.sensitivity

        if (abs(ydif) > self.threshold):
            dy = (ydif + 1.5)* dt * self.ratio_y * self.sensitivity

        return dx, dy

    def getXY(self, data):
        xyz = data.decode().split('|')
        x = float(xyz[0]) * -1
        y = float(xyz[1]) * -1
        self.left = int(xyz[2])
        self.middle = int(xyz[3])
        self.right = int(xyz[4])

        return x, y

    def popData(self):
        #print(len(self.dts))
        if len(self.data) >= 3:
            x1, y1 = self.getXY(self.data.pop())
            x2, y2 = self.getXY(self.data.pop())
            x3, y3 = self.getXY(self.data.pop())

            t1 = self.dts.pop()
            t2 = self.dts.pop()
            t3 = self.dts.pop()
            
            

            return [x1 + x2 + x3 / 3.0, y1 + y2 + y3 / 3.0, t1 + t2 + t3 / 3.0]
        else:
            return 0,0,0

    def handleMouseClicks(self):
        if self.left == 1 and self.leftState == 0:
            self.mouse.click(Button.left, 1)
            self.leftState = 1
        elif self.left == 0:
            self.leftState = 0
        if self.middle == 1 and self.middleState == 0:
            self.middleState = 1
        elif self.middle == 0:
            self.middleState = 0
        if self.right == 1 and self.rightState == 0:
            self.mouse.click(Button.right, 1)
            self.rightState = 1
        elif self.right == 0:
            self.rightState = 0

    def handleMouse(self):
        while(True):
            x, y, dt = self.popData()

            mx, my = self.handleMouseCoords(x, y, dt)
            self.handleMouseClicks()
            #print("threaded: ", mx, " ", my)
            if (self.middleState == 0):
                mouse.move(mx, my, absolute=False, duration=dt)
            else:
                self.mouse.scroll(0.02*x, 0.02*y)
 
class NotificationDelegate(DefaultDelegate):

    def __init__(self, wand):
        DefaultDelegate.__init__(self)
        self.t = 0
        self.wand = wand

    def handleNotification(self, cHandle, data):
        dt = 0
        if (self.t == 0):
            self.t = time.time()
            dt = 0.1
        else:
            last_time = self.t
            self.t = time.time()
            dt = self.t - last_time

        self.wand.pushData(data, dt)

try:
    service = DiscoveryService()
    print("Connecting to device...")
    devices = service.discover(2)
    dev_add = ""
    for address, name in devices.items():
        if name == "AirWand":
            dev_add += address
    arduino = Peripheral(deviceAddr=dev_add)

    wand = Wand()

    arduino.setDelegate(NotificationDelegate(wand))
    print("Connected to AirWand.")
except BTLEDisconnectError as error:
    print(error)
    print("Peripheral not found. Is it ON?")
else:
    #services = arduino.getServices()
    characteristics = arduino.getCharacteristics()

    for characteristic in characteristics:
        try:
            print(f"Value: {characteristic.read().decode()}, UUID: {characteristic.uuid}, Handle: {characteristic.handle}, ValueHandle: {characteristic.valHandle}")
        except:
            pass
    #start_notification_data = b"\x01\x00"
    arduino.writeCharacteristic(13, bytes([1])) # arduino.writeCharacteristic(13, start_notification_data) will also work, 13 comes from "characteristic handle + 1"

    start_new_thread(wand.handleMouse, (),)

    while True:
        try:
            if arduino.waitForNotifications(2):
                # handleNotification() gets called
                continue
            print("Waiting...")
        except KeyboardInterrupt:
            break
 
    arduino.disconnect()
