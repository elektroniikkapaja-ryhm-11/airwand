#include "Arduino.h"
#include <ArduinoBLE.h>
#include "Nano33BLEGyroscope.h"

#define BTBufferSize  1000
#define BTDeviceName   "Arduino Nano 33 BLE"
#define BTLocalName    "AirWand"

Nano33BLEGyroscopeData gyroscopeData;
BLEService BLEGyroscope("590d65c7-3a0a-4023-a05a-6aaf2f22441c");
BLECharacteristic gyroscopeXBLE("0004", BLERead | BLENotify | BLEBroadcast, bufferSize);

char bleBuffer[BTBufferSize];

const int redPin = 22;
const int greenPin = 23;
const int bluePin = 24;
const int threshold = 70;
const int leftPin = 2;
const int scrollPin = 3;
const int rightPin = 4;

int leftState = 0;
int scrollState = 0;
int rightState = 0;

void setup()
{   
    pinMode(redPin, OUTPUT);
    pinMode(greenPin, OUTPUT);
    pinMode(bluePin, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(leftPin, INPUT);
    pinMode(scrollPin, INPUT);
    pinMode(rightPin, INPUT);
    digitalWrite(redPin, LOW);
    digitalWrite(greenPin, HIGH);
    digitalWrite(bluePin, HIGH);
     
    Serial.begin(1000000);
    while (!BLE.begin()) {}
    BLE.setDeviceName(BTDeviceName);
    BLE.setLocalName(BTLocalName);
    BLE.setAdvertisedService(BLEGyroscope);
    BLEGyroscope.addCharacteristic(gyroscopeXBLE);
    
    BLE.addService(BLEGyroscope);
    BLE.advertise();
    Gyroscope.begin();

    delay(2000);
    digitalWrite(redPin,HIGH);
}

void loop()
{
  digitalWrite(bluePin, LOW);
  BLEDevice central = BLE.central();
  
  if(central)
  {
    int writeLength;
    while(central.connected())
    {
      
      leftState = (digitalRead(leftPin) == HIGH) ? 1 : 0;
      scrollState = (digitalRead(scrollPin) == HIGH) ? 1 : 0;
      rightState = (digitalRead(rightPin) == HIGH) ? 1 : 0;
      
      digitalWrite(bluePin, HIGH);
      if (leftState == 1) {
        digitalWrite(greenPin, LOW);
      } else {
        digitalWrite(greenPin, HIGH);
      }

      if (rightState == 1) {
        digitalWrite(redPin, LOW);
      } else {
        digitalWrite(redPin, HIGH);
      }
      
      if(Gyroscope.pop(gyroscopeData))
      {
        writeLength = sprintf(bleBuffer, "%d|%d|%d|%d|%d", (int)gyroscopeData.z, (int)gyroscopeData.y, leftState, scrollState, rightState);
        gyroscopeXBLE.writeValue(bleBuffer, writeLength);
      }
    }
  }
}
